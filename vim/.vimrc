
let mapleader = "."
let maplocalleader = "\\"

nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

nnoremap <leader>vs :vsplit<cr>
nnoremap <leader>sp :split<cr>

" Enclosing text in quotes
nnoremap <leader>" viw<esc>a"<esc>bi"<esc>lel
nnoremap <leader>' viw<esc>a'<esc>bi'<esc>lel


" Remap jk to ESC and disable ESC button(good to force this change)
inoremap jk <ESC>
inoremap <ESC> <Nop>

" Better window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l


" ========= Disable arrow keys ============
no <down> <Nop>
no <left> <Nop>
no <right> <Nop>
no <up> <Nop>

ino <down> <Nop>
ino <left> <Nop>
ino <right> <Nop>
ino <up> <Nop>

vno <down> <Nop>
vno <left> <Nop>
vno <right> <Nop>
vno <up> <Nop>

syntax enable

set nocompatible
set autoread

" No backups
set viminfo='0,:0,<0,@0,f0
set nobackup
set nowb
set noswapfile

" Indent
set ai
set si

" Show file options above the command line
set wildmenu

" Don't offer to open certain files/directories
set wildignore+=node_modules/*,bower_components/*

" Allow backspace over eveyrthing in insert mode
set backspace=indent,eol,start

set ruler
set showcmd

" Don't use Ex mode, use Q for formatting
map Q gq

" set the working directory to wherever the open files lives
set autochdir


"  Set search and highlighting
set incsearch
" set hlsearch

" Ignore case when searching
set ignorecase
set smartcase

" show matching brackets
set showmatch

" show cursor line
set cursorline

" change netrw tree mode
let g:netrw_liststyle=3

" set default vertical split to right
set splitright
"set list
set nu

let g:solarized_termcolors=256

set number
set relativenumber

set background=light

colorscheme solarized

" Handle Indentation here
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set autoindent
set textwidth=80

" FINDING FILES:

" Search down into subfolders
" Provides tab-completion for all file-related tasks
" set path+=.,**
set path=$PWD/**
set wildignore +=$PWD/node_modules/**



" Maintain undo history between sessions
" set undofile
" Set maximum number of changes that can be undone
" set undolevels
